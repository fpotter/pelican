#!/usr/bin/env python
# -*- coding: utf-8 -*- #

# Used for both local dev and CI-based publishing.

AUTHOR = 'GitLab'
SITENAME = 'Example Pelican website using GitLab Pages!'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Athens'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('GitLab', 'https://gitlab.com/'),
         ('Pages Documentation', 'https://docs.gitlab.com/ee/user/project/pages/'),)

# Social widget
SOCIAL = (('GitLab on Facebook', 'https://facebook.com/gitlab'),
          ('GitLab on LinkedIn', 'https://linkedin.com/company/gitlab-com'),)

DEFAULT_PAGINATION = 10

DISPLAY_PAGES_ON_MENU = False


# When developing locally, use the git clone command in the README
# to get the theme collection before uncommenting the THEME line
# and experimenting with themes.

# THEME = '.themes/simple-bootstrap'