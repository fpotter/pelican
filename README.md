# Pelican on GitLab Pages

Example [Pelican] website using GitLab Pages. Check the resulting website here: <https://pages.gitlab.io/pelican>

Learn more about [GitLab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/) and the [official
documentation](https://docs.gitlab.com/ce/user/project/pages/), including
[how to fork a project like this one to get started](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_two.html#fork-a-project-to-get-started-from).

---

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in the file [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Pelican
1. Regenerate and serve on port 8000: `make devserver`
1. Add content

Read more at Pelican's [documentation].

## GitLab User or Group Pages

To use this project as your user/group website, you will need to perform
some additional steps:

1. Rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings > General (Advanced)**.

2. Adjust Pelican's `SITEURL` configuration setting in `publishconf.py` to
the new URL (e.g. `https://namespace.gitlab.io`)

Read more about [GitLab Pages for projects and user/group websites][pagesdoc].

## Change the theme

_Pelican's `pelican-themes` CLI patches the Pelican package to add the theme. A more CI-friendly approach is to store themes in a gitignored subdirectory of the project._

### Use a theme from the published collection

Locally:

```bash
git clone git clone https://github.com/getpelican/pelican-themes.git .themes
```

Then in your pelicanconf.py, change `THEME` to point to a subdirectory of `.themes`.

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Documentation

- [GitLab Pages documentation](https://docs.gitlab.com/ee/user/project/pages/)
- [GitLab CI/CD](https://about.gitlab.com/gitlab-ci/)
- [Pelican](http://blog.getpelican.com/)
- [Pelican installation](https://docs.getpelican.com/en/stable/install.html)
- [Pelican documentation](http://docs.getpelican.com/)
