Title: Pelican on GitLab Pages!
save_as: index.html

This site is hosted on GitLab Pages!

The source code of this site is at <https://gitlab.com/pages/pelican>.

Learn about GitLab Pages at <https://pages.gitlab.io>.
