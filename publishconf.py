#!/usr/bin/env python
# -*- coding: utf-8 -*- #

# This file is only used if you use `make publish` or
# explicitly specify it as your config file. It's used
# by default from GitLab CI/CD for GitLab Pages, and
# includes the contents of pelicanconf.py.

import os
import sys
sys.path.append(os.curdir)
from pelicanconf import *


# Subdirectories are the default behaviour for GitLab
# Pages. Change SITEURL to '/' if using a custom domain.
# Or set RELATIVE_URLs, though absolute URLs are generally
# considered best practice.

SITEURL = '/pelican'
RELATIVE_URLS = False


# The ATOM support referenced here seems to break

# FEED_ALL_ATOM = 'feeds/all.atom.xml'
# CATEGORY_FEED_ATOM = 'feeds/{slug}.atom.xml'


# Clean up old content

DELETE_OUTPUT_DIRECTORY = True


# Following items are often useful when publishing

#DISQUS_SITENAME = ""
#GOOGLE_ANALYTICS = ""

